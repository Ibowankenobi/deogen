#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  SETUP.py
#  
#  Copyright 2016 Daniele Raimondi <eddiewrc@alice.it, daniele.raimondi@vub.ac.be>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import sys, os


COMPILE_BLAST=False
ARCHITECTURE_BLAST=64

def main():
	print "*****************************************************"
	print "Welcome to DEOGEN installation program. \nThis is recommended ONLY for VERY INEXPERIENCED users!"
	print "Othwerwise you can follow the instructions in the README file."
	print "*****************************************************"
	print "Press any key to start the installation:"
	raw_input()
	
	print "*****************************************************"
	print "Creating Folders (if they already exist you may have another installation)..."
	os.system("mkdir deogenAUTO ; cd deogenAUTO")
	os.system("mkdir thirdParty ; cd thirdParty")
	######################################CDHIT installation
	print "*****************************************************"
	print "Do you wish to install CD-HIT? Input N if you already have your own installation. (y/n)"
	ans = raw_input()
	while ans.lower() != "n" and ans.lower() != "y":
		print "Please enter Yes or No (y or n)"
		ans = raw_input()
	if ans.lower() == "y":
		print "Downloading CD-HIT 4.5.4..."
		os.system("cd thirdParty; wget --tries=2 www.bioinformatics.org/downloads/index.php/cd-hit/cd-hit-v4.5.4-2011-03-07.tgz -O cd-hit-4.5.4.tgz")
		print "Done."
		print "Extracting and compiling CD-HIT..."
		out = os.system("cd thirdParty; tar -xf cd-hit-4.5.4.tgz ; mv cd-hit-v4.5.4-2011-03-07 cd-hit-4.5.4 ")
		if out == 0:
			print "Extraction OK!"
		else:
			print "ERROR!"
		out = os.system("cd thirdParty/cd-hit-4.5.4; make")
		if out == 0:
			print "Successful compilation. OK!"
		else:
			print "\n\n***************************************************************\n"
			print "ERROR! CD-HIT 4.5.4 (recommended version) appears to have problems on your system. I'm now trying with the latest version, 4.6.4..."
			print "\n***************************************************************\n\n"
		print "Downloading CD-HIT 4.6.4..."
		os.system("cd thirdParty; wget --tries=2 https://github.com/weizhongli/cdhit/archive/V4.6.4.tar.gz -O cd-hit-4.6.4.tgz")
		print "Done."
		print "Extracting and compiling CD-HIT..."
		out = os.system("cd thirdParty; tar -xf cd-hit-4.6.4.tgz ; mv cdhit-4.6.4 cd-hit-4.6.4 ")
		if out == 0:
			print "Extraction OK!"
		else:
			print "ERROR!"
		out = os.system("cd thirdParty/cd-hit-4.6.4; make")
		if out == 0:
			print "Successful compilation. OK!"
			print "Removing old version..."
			os.system("cd thirdParty; rm -rf cd-hit-4.5.4 cd-hit-4.5.4.tgz")
			print "Done."
		else:
			print "ERROR! CD-HIT 4.6.4 appears ALSO to have problems on your system. Try to compile them manually! :("
			return "CD-HIT error!"	
	else:
		"Skipping CD-HIT installation!"
	###########################################BLAST INSTALLATION
	print "*****************************************************"
	print "Do you wish to install NCBI BLAST? Input N if you already have your own installation. (y/n)"
	ans = raw_input()
	while ans.lower() != "n" and ans.lower() != "y":
		print "Please enter Yes or No (y or n)"
		ans = raw_input()
	if ans.lower() == "y":
		print "Do you want to COMPILE it or install the 64-bits binaries? Compilation can be very long. (input C for Compile, B for Binaries)"
		ans = raw_input()
		while ans.lower() != "c" and ans.lower() != "b":
			print "Please enter Compile od Binaries (C or B)"
			ans = raw_input()
		if ans.lower() == "c":
			if COMPILE_BLAST == True: #quite long
				print "Downloading BLAST..."
				os.system("cd thirdParty; wget --tries=2 ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/ncbi-blast-2.3.0+-src.tar.gz -O ncbi-blast-2.3.0+-src.tar.gz")
				print "Done."	
				print "Extracting and compiling BLAST..."
				out = os.system("cd thirdParty; tar -xf ncbi-blast-2.3.0+-src.tar.gz ; mv ncbi-blast-2.3.0+-src ncbi-blast-2.3.0+ ")
				if out == 0:
					print "Extraction OK!"
				else:
					print "ERROR!"
				print "Running configure! (You'll see a lot of output!)"
				out = os.system("cd thirdParty/ncbi-blast-2.3.0+/c++ ; ./configure")
				if out != 0:
					print "\n ERROR during configuration, please fix it and then restart SETUP.py!\n"
					return "BLAST configure error"
				print "Compiling (it will take a while!)..."
				out = os.system("cd thirdParty/ncbi-blast-2.3.0+/c++ ; make")
				if out == 0:
					print "Successful compilation. OK!"
					#print "Removing old version..."
					#os.system("cd thirdParty; rm -rf cd-hit-4.5.4 cd-hit-4.5.4.tgz")
					#print "Done."
				else:
					print "ERROR! ncbi-blast-2.3.0+ appears to have problems on your system. Try to compile them manually! :("
					return "BLAST make error!"	
		else:
			print "Downloading BLAST..."
			os.system("cd thirdParty; wget --tries=2 ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/ncbi-blast-2.3.0+-x64-linux.tar.gz -O ncbi-blast-2.3.0+.tar.gz")
			print "Done."	
			print "Extracting BLAST binaries..."
			out = os.system("cd thirdParty; tar -xf ncbi-blast-2.3.0+.tar.gz ; ")
			if out == 0:
				print "Extraction OK!"
			else:
				print "ERROR while extracting BLAST!"
				return "BLAST extraction error!"
	
	####################################################HMMER3 installation			
	print "*****************************************************"
	print "Do you wish to install HMMER3? Input N if you already have your own installation. (y/n)"
	ans = raw_input()
	while ans.lower() != "n" and ans.lower() != "y":
		print "Please enter Yes or No (y or n)"
		ans = raw_input()			
		
	if ans.lower() == "y":
		print "Downloading HMMER..."
		os.system("cd thirdParty; wget --tries=2 eddylab.org/software/hmmer3/3.1b2/hmmer-3.1b2.tar.gz -O hmmer-3.1b2.gz")
		print "Done."
		print "Extracting and compiling HMMER..."
		out = os.system("cd thirdParty; tar -xf hmmer-3.1b2.gz ; ")
		if out == 0:
			print "Extraction OK!"
		else:
			print "ERROR!"
		print "Running configure! (You'll see a lot of output!)"
		out = os.system("cd thirdParty/hmmer-3.1b2 ; ./configure")
		if out != 0:
			print "\n ERROR during configuration, please fix it and then restart SETUP.py!\n"
			return "HMMER configure error"
		print "Compiling..."
		out = os.system("cd thirdParty/hmmer-3.1b2 ; make")
		if out == 0:
			print "Successful compilation. OK!"
		else:
			print "ERROR! hmmer-3.1 appears to have problems on your system. Try to compile them manually! :("
			return "HMMER make error!"
		
	
	print "*****************************************************"
	print "Do you wish to download BLAST DATABASE? Input N if you already have your own blast database. (y/n)"
	ans = raw_input()
	while ans.lower() != "n" and ans.lower() != "y":
		print "Please enter Yes or No (y or n)"
		ans = raw_input()
	if ans.lower() == "y":
		os.system("mkdir thirdParty/blastDB")
		print "Downloading BLAST DATABASE (it can take a while)..."
		os.system("cd thirdParty/blastDB ; wget --retry=0 --retry-connrefused ftp://ftp.jcvi.org/pub/data/provean/nr_Aug_2011/nr.*.tar.gz")
		print "Done"
	print "*****************************************************"
	print "Do you wish to download UNIREF100 database? Input N if you already have your own Uniref100 copy. (y/n)"
	ans = raw_input()
	while ans.lower() != "n" and ans.lower() != "y":
		print "Please enter Yes or No (y or n)"
		ans = raw_input()
	if ans.lower() == "y":
		print "Downloading Uniref100 (it can take a while)..."
		os.system("cd thirdParty ; wget --retry=0 --retry-connrefused ftp://ftp.uniprot.org/pub/databases/uniprot/uniref/uniref100/uniref100.fasta.gz")
		print "Done.\nExtracting it..."
		os.system("cd thirdParty ; gunzip uniref100.fasta.gz")
		print "Done"
		
	print "Installation completed! Please read carefully the README to finalize it!"	
	return 0

if __name__ == '__main__':
	main()
