#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  deogen-all.py
#  
#  Copyright 2015 Daniele Raimondi <eddiewrc@alice.it, daniele.raimondi@vub.ac.be>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import sources.DRandomForests
import sources.pathwayUtils
import sources.deogenUtils as dUtils
from localConfig import *
import os, sys, cPickle

def main():
	print "************************************************************"
	print "*  Deogen-all 1.1   (April 2016)                           *"
	print "*  Raimondi, Gazzo, Rooman, Lenaerts and Vranken           *"
	print "*                                                          *"
	print "*  doi: 10.1093/bioinformatics/btw094                      *"
	print "************************************************************"
	
	if len(sys.argv) < 2 or "-h" in sys.argv[1] or ("-s" not in sys.argv[1] and "-u" not in sys.argv[1]):
		print "\n\nUSAGE: deogen-all.py -u INPUT_FILE"
		print "\n\n"
		exit(1)
			
	inFile = sys.argv[2]
	if not os.path.exists(inFile):
		print "ERROR: the specified input file %s doesn't exist! "%inFile
		exit(3)
	print "Creating folders if needed...",
	os.system("mkdir -p "+WORKING_DIR+" "+DOWNLOAD_DIR+" "+JH_MSA_FOLDER +" "+RESULTS_DIR)
	print "Done."		
	print "Check PROVEAN installation...",
	t = os.system(PROVEAN_SH+" --help  >> /dev/null")
	if t != 0:
		print "\nERROR: "+ PROVEAN_SH + " is not present or not working. Please set the correct path in localConfig.py and install PROVEAN. (see README)\n"
		exit(4)
	print "Done."	
	print "Reading REC, ESS and DGR annotations...",
	nfsp = cPickle.load(open("DATA/dbNFSP.cPickle"))
	assert nfsp != None and nfsp.keys() > 1
	print "Done."
	print "Reading pathway log-odd scores...",
	P = cPickle.load(open("DATA/pathway.cPickle"))
	assert P != None	
	print "Done."
	print "Reading RandomForest trained modelSNVss...",
	modelSNVs = cPickle.load(open("DATA/modelSNVs.cPickle"))
	modelIndels = cPickle.load(open("DATA/modelINDELs.cPickle"))
	if modelSNVs == None or modelIndels == None:
		print "\nERROR occurred while loading learned models. Please make sure the .cPickle files are present in DATA/ folder!\n"
		exit(5)
	print "Done."
	os.system("mkdir -p "+TMP_DIR)
	################## START COLLECTING FEATURES!
	
	
	if sys.argv[1] == "-u":	
		print "Reading Uniprot IDs-based input file %s..." %(inFile),
		sys.stdout.flush()
		varList = dUtils.readInputFileUIDs(inFile)
		#print varList["Q8NCM8"]
		#raw_input()
		print "Done."		
		print "\nRetrieved %d variants on %d proteins. Starting Deogen!" % (len(varList.items()), len(varList.keys()))
		sys.stdout.flush()
		totVar = len(varList.items())
		
		for prot in varList.items():
			X = {}	
			print "\n > Working on protein %s" % prot[0]
			print "  >> Downloading sequence from UniProt..."
			sys.stdout.flush()
			if not os.path.exists(DOWNLOAD_DIR+prot[0]+".fasta"):
				t = os.system("wget -t "+str(RETRY)+" --retry-connrefused --directory-prefix="+DOWNLOAD_DIR+" http://www.uniprot.org/uniprot/"+prot[0]+".fasta > /dev/null")
				if t != 0:
					print "\nERROR: Problem during downloading %s fasta file from Uniprot. Skipping!" % (mut)
					sys.stdout.flush()
					continue			
			
			print "  >> Check variant consistency...",
			tseq = dUtils.readFASTA(DOWNLOAD_DIR+prot[0]+".fasta")[0][1]
			#print tseq			
			for mut in prot[1]:		
				w, pos, m = dUtils.translateMutationPROVEAN(mut)	#it's reading indels wrongly but don't worry!				
							
				if w != tseq[pos]:
					print "\nWARNING: Reference AA does not match: %s in uniprot sequence, but %s is provided at position %d" % (tseq[pos], w, pos)
					print "WARNING: DEOGEN will replace %s with %s as wildtype AA! New variant: %s%d%s\n *** THIS MAY GIVE MISLEADING PREDICTIONS, please check this issue! *** \n" % (w, tseq[pos], tseq[pos], pos, m)
					tmp = varList[prot[0]]
					tmp.remove(mut)
					tmp.append("%s%d%s" % (tseq[pos], pos+1, m))
					varList[prot[0]] = tmp					
							
			print "Done.\n  >> Starting PROVEAN... (it may take some minutes) ",
			sys.stdout.flush()
			dUtils.writeProvVar(prot[1], WORKING_DIR+prot[0]+".var")	
			if not os.path.exists(WORKING_DIR+prot[0]+".prov") or os.stat(WORKING_DIR+prot[0]+".prov").st_size == 0 or not dUtils.isAlreadyPROVpredicted(dUtils.readProvPreds(WORKING_DIR+prot[0]+".prov"), prot[1]):
				if SAVE_SUPPORTING:
					if os.path.exists(WORKING_DIR+prot[0]+".support"):
						os.system(PROVEAN_SH+" -q "+DOWNLOAD_DIR+prot[0]+".fasta -v "+WORKING_DIR+prot[0]+".var --supporting_set "+WORKING_DIR+prot[0]+".support"+" --num_threads "+str(CORES)+" --tmp_dir "+ TMP_DIR +" > "+WORKING_DIR+prot[0]+".prov")
					else:	
						os.system(PROVEAN_SH+" -q "+DOWNLOAD_DIR+prot[0]+".fasta -v "+WORKING_DIR+prot[0]+".var --save_supporting_set "+WORKING_DIR+prot[0]+".support"+" --num_threads "+str(CORES)+" --tmp_dir "+ TMP_DIR +" > "+WORKING_DIR+prot[0]+".prov")
				else:
					os.system(PROVEAN_SH+" -q "+DOWNLOAD_DIR+prot[0]+".fasta -v "+WORKING_DIR+prot[0]+".var --num_threads "+str(CORES)+" --tmp_dir "+ TMP_DIR +" > "+WORKING_DIR+prot[0]+".prov")				
				print "Done."
			else:
				print "PROVEAN predictions already computed!"
			sys.stdout.flush()
			tmp =  dUtils.readProvPreds(WORKING_DIR+prot[0]+".prov")
			if tmp == -1:
				print " ERROR: PROVEAN experienced some problems, no predictions!!!"
				return -1
			if not X.has_key(prot[0]):
				X[prot[0]] = {}
			for i in tmp:
				if i[0] in prot[1]:
					X[prot[0]][i[0]] = [float(i[1])]
			
			#retrieving JH MSAs ###############################
			
			sys.stdout.flush()	
				
			if not os.path.exists(JH_MSA_FOLDER+prot[0]+MSA_FILT_SUFFIX) or os.stat(JH_MSA_FOLDER+prot[0]+MSA_FILT_SUFFIX).st_size == 0:
				print "  >> Building JackHmmer alignment...",
				sys.stdout.flush()	
				t = os.system(JH_BIN+" --acc --notextw --noali -E "+str(JH_EVALUE)+" -N "+str(JH_ITER)+" --cpu "+str(CORES)+" -A "+JH_MSA_FOLDER+prot[0]+".sto "+DOWNLOAD_DIR+prot[0]+".fasta"+" "+JH_DATABASE+" > /dev/null")
				if t != 0:
					print "\nERROR: jackHmmer encountered an error!"
					sys.stdout.flush()
					exit(5)
				print "Done."
				sys.stdout.flush()
				print "  >> Reformatting and reading JackHmmer alignment..."
				sys.stdout.flush()
				os.system("./sources/reformat.pl sto fas "+JH_MSA_FOLDER+prot[0]+".sto "+" "+JH_MSA_FOLDER+prot[0]+".fas > /dev/null")
				os.system("./sources/trim.py "+JH_MSA_FOLDER+prot[0]+".fas"+" $(head -n 1 "+JH_MSA_FOLDER+prot[0]+".fas"+" | cut -f2 -d \">\") > "+JH_MSA_FOLDER+prot[0]+".hmmer")	
				filteredMsa = dUtils.filterMSA(JH_MSA_FOLDER+prot[0]+".hmmer", SI, COV)
				dUtils.buildFasta(filteredMsa, JH_MSA_FOLDER+prot[0]+MSA_FILT_SUFFIX)
				print "  >> Cleaning MSAs...",
				os.system("rm "+JH_MSA_FOLDER+prot[0]+".sto "+JH_MSA_FOLDER+prot[0]+".fas "+JH_MSA_FOLDER+prot[0]+".hmmer")
				print "Done."
			else:
				print "  >> JH alignment already built and filtered!"
			#os.system("rm "+i[0] +" "+ fileName+"HMMERAlignDir/"+alignName+".sto " +" "+ fileName+"HMMERAlignDir/"+alignName+".fas " )
				
			print "  >> Collecting CI and LOR features..."
			sys.stdout.flush()			
			if os.path.exists(JH_MSA_FOLDER+prot[0]+MSA_FILT_SUFFIX):
				msa = dUtils.readMSA(JH_MSA_FOLDER+prot[0]+MSA_FILT_SUFFIX)
			else:
				print "\n ERROR: %s MSA is not present!" % (JH_MSA_FOLDER+prot[0]+MSA_FILT_SUFFIX)
				exit(6)
			if len(msa) == 0:
				print "\nWARNING: CI and LOR predictions for %s not available due to empty MSA. Skipping!\n" % prot[0]
				del X[prot[0]]
				continue
			else:
				print "  >> Read %d sequences." % len(msa)
				
			freqsTot = dUtils.computeMSAFreqs(msa)
			for mut in prot[1]:					
				if "del" in mut or "ins" in mut or "delins" in mut:
					print "  >>>> INDEL: "+mut
					CI = dUtils.getProbMutIndel(msa, mut, freqsTot)							
					X[prot[0]][mut] += [CI]
				else:	
					print "  >>>> SNV: "+mut
					w, pos, m = dUtils.translateMutationPROVEAN(mut)				
					CI, prob = dUtils.getProbMut(msa, pos, (w,m), freqsTot)								
					X[prot[0]][mut] += [CI, prob]
			
			print "  >> Done."
			sys.stdout.flush()			
			
			print "  >> Collecting pathway log-odd scores...",
			sys.stdout.flush()
			
			tmpP = [P.getLogOdd(prot[0])]
			for mut in prot[1]:
				X[prot[0]][mut] += tmpP
			
			print "Done."
			sys.stdout.flush()
			
			print "  >> Collecting DGR, REC and ESS features...",
			sys.stdout.flush()			
			
			for mut in prot[1]:
				X[prot[0]][mut] += dUtils.selectNFSPfeatures(nfsp, prot[0])
				
			print "Done."
			sys.stdout.flush()
			#print X
			print "  >> Computing DEOGEN predictions for prot %s..." % (prot[0])
			Xsnvs = {}
			Xindels = {}
			for pr in X.items():
				#print pr
				for mu in pr[1].items():
					#print mu
					if len(mu[1]) == 7: #SNV
						if not Xsnvs.has_key(pr[0]):
							Xsnvs[pr[0]] = {}
						Xsnvs[pr[0]][mu[0]] = mu[1]
					elif len(mu[1]) == 6:
						if not Xindels.has_key(pr[0]):
							Xindels[pr[0]] = {}
						Xindels[pr[0]][mu[0]] = mu[1]
					else:
						raise Exception("!!!! ERROR: %s has a wrong number of features!" % mu)

			print "  >>> Found %d SNVs and %d INDELs" % (len(Xsnvs.keys()),len(Xindels.keys()))	,
			xsnvs = []
			correspSnvs = []
			ysnvs = []
			xindels = []
			correspIndels = []
			yindels = []
			if len(Xsnvs.keys()) > 0:	
				xsnvs, correspSnvs = dUtils.hash2feats(Xsnvs)
				#print xsnvs, correspSnvs
				ysnvs = list(modelSNVs.decision_function(xsnvs))
			if len(Xindels) > 0:
				xindels, correspIndels = dUtils.hash2feats(Xindels)
				#print xindels, correspIndels
				yindels = list(modelIndels.decision_function(xindels))#numpy array returned!
			print "Done"
			sys.stdout.flush()
			#print ysnvs, yindels, ysnvs+yindels
			#print correspSnvs, correspIndels, correspSnvs+correspIndels
			
			print "  >>  Saving predictions in: "+RESULTS_DIR+prot[0]+".deogen.predictions *** ...",
			dUtils.writeResults(ysnvs+yindels, correspSnvs+correspIndels, RESULTS_DIR+prot[0]+".deogen.predictions")
			dUtils.writeFeatures(xsnvs+xindels, correspSnvs+correspIndels, RESULTS_DIR+prot[0]+".deogen.features")
			print "Done.\n"
	print "Done.\nE infine uscimmo a riveder le stelle.",
	sys.stdout.flush()		
	return 0

if __name__ == '__main__':
	main()

